# GSOC-2019
Here's where I will collect my drafts and ideas for this GSOC 2019


## Mentorship-System Extension
### Presentation
[Mentorship System Extension](https://docs.google.com/presentation/d/e/2PACX-1vRd-z28ROAlCQDccZv2ix5RuK8MCbz6b3RkD9iPUHF7GGIXEj-mDE-LX8GM2JYscCOsuRHBlNA2Tjyy/pub?start=false&loop=false&delayms=10000#slide=id.p)

### Detailed Writeup:
[Mentorship System Pitch](https://github.com/haroldadmin/GSOC-2019/blob/master/Mentorship-Extension/Mentorship%20System%20Extension%20Pitch.md)

## Rosewood
### Presentation
[Rosewood Presentation](https://docs.google.com/presentation/d/e/2PACX-1vRL6EVeGwZQisf_mr4-9hGbMuH1OnS-OHz0mRZ6l1K7EOQHtk5yRysa9gLbOi3lKXD7BBiV9DkonntB/pub?start=false&loop=false&delayms=10000)

### Detailed Writeup
[Detailed writeup](https://github.com/haroldadmin/GSOC-2019/blob/master/Rosewood/Detailed%20Writeup.md)

### Market Research
[Market Research](https://github.com/haroldadmin/GSOC-2019/blob/master/Rosewood/Market%20Research.md)

### Questions and Feedback welcome!
