# Mentorship System Extension Pitch
#gsoc #mentorship

## Goal: Make it easier to find mentors and mentees according to selected preferences
*I am a GSOC 2019 aspirant. I want to work on the Mentorship project of the Systers organisation.*

The Mentorship project was proposed by Isabel Costa in GSOC 2018, with the goal of helping people find mentors easily. Solutions for this problem already exist, but what makes the Mentorship system unique is its volunteer based approach. The Mentorship system is aimed at anyone who needs a mentor, regardless of the field they need mentoring in. Isabel’s efforts helped create the Mentorship system from scratch. As of now, we have a backend server with a lot of existing functionality, and a budding Android app that has a lot of potential.

I want to work on extending and polishing the Mentorship system, with the goal of making it easier to find Mentors and Mentees according to a user’s specific preferences. I have compiled a list of various things that can be improved in this project, and the ways I suggest we improve them:

1. Make the process of finding mentors and mentees in the members list easier -> Filter members list according to various new and existing options
2. Polish the user interface of the app -> Material Components, Consistent theming, Animations, Profile Pictures
3. Core architecture of the app -> Dependency Injection through ~~Dagger 2~~ Koin, A persistent ViewModel to cache frequently accessed data

### Make it easier to find mentors and mentees
The Mentorship system aims at being welcoming for everyone, and that means it should be able to serve people who speak languages other than English. It should also make it easy for users to narrow down the list of members available according to what they are looking for. 

* Make profiles richer with information such as Spoken Languages and Skills (each skill would be represented as a separate entity, instead of a string)
* Communication language is often a barrier between people, and therefore they should be able to add what languages they speak in their profiles. This would also allow filtration on the basis of these languages, meaning you can find mentors/mentees who can communicate in your language.
* A badge of honour for mentors on our platform which they could use on their LinkedIn or other social network profiles. 
* Filtering the members list based on location proximity


Let’s look at this through an example. Say a user named *Harold* is skilled at public speaking, but only knows Spanish. He wants to be a mentor on our platform, and is looking for mentees who might find his skill useful. Harold should be able to look at the members list and filter it so that it only shows users that:
		* Need Mentoring
		* Need a mentor who is skilled at public speaking
		* Can speak Spanish

The features I propose would make this possible. It requires work both on the backend as well as the app.

### Polishing the user interface:
The app should be aesthetically pleasing and easy to use to welcome new users. It should work fast, and keep user’s data secure. I want to propose the use of Google’s Material Design Components for Android along with fluent Animations to make the user experience smooth. Since a big part of the app is our user’s profile, I also want to build the profile pictures feature which would make the app instantly more personalised. Profile pictures can be used in several places in the UI, like the homepage and members list. I have created some rough mockups of how this might look like in a redesigned version of the mentorship app.

* Material Design Components
* Animations
* Profile Pictures
* Consistent theme throughout the app to establish a unique Identity of our platform

### Core Architecture
The app needs to be scalable as it grows. It currently follows the MVVM architectural pattern, which is an excellent starting point. I want to integrate a dependency injection system like ~~Dagger~~ Koin, so that we can make the app more scalable and testable. 
I also want to introduce the use of Kotlin’s sealed classes to model UI state. Sealed classes enable a much cleaner way of handling state representation, and since our project is 100% Kotlin already we should aim for embracing more of its features. 
I also want to introduce an Activity scoped ViewModel which could cache frequently accessed information like the user’s profile, so that the user does not have to wait for information to load every time they switch to a different screen in the app.

* Dependency Injection
* Sealed Classes to model state
* Activity Scoped ViewModel to cache information

### Some other miscellaneous but nice to have features:
#### Filtering the members list based on location proximity:
	
	* This would allow users to find mentors/mentees located near them.
	* This could allow in-person interaction between users. We have to proceed with caution here because we need to make sure that people registered on the app mean no harm to others.
	
#### A badge of honour
A badge of honour for mentors on our platform which they could use on their LinkedIn or other social network profiles. A badge would represent proof that this user has mentored X amount of people on our platform.
	
	* We could game-ify this by providing different types of badges based on the number of people successfully mentored
	* It could also be used to make community ranking system to provide incentive for mentors to keep up their work
	* Users displaying this badge on their profile would attract more people to our platform

## Unique Ideas in my proposal:
1. Filter Members list according to:
	* Language
	* Skills
	* Location Proximity
2. Improved user interface using Material Design Components and Animations
3. Improved core architecture of the app using sealed classes, DI, and caching ViewModel
4. Gamification of the mentoring system for badges on Social Networks and Community Ranking scheme

I have been contributing to Systers for the past few months and I love the community for it’s efforts to help women in technology. I believe the Mentorship System can be a flagship project for Systers and has the potential to help a lot of people.
Please let me know if you have any questions or feedback regarding my ideas.

## Ideas Under Consideration:
1. Support for banning users through the Admin API
2. Rating/Review System for Mentors (Although community rankings could be a substitute for this)

You can find me at these places:

Twitter: @haroldadmin

Github: haroldadmin

Reddit: /u/theharolddev
