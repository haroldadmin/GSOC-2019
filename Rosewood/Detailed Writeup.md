# Rosewood

Modern Smartphones are an indispensable resource in the today’s world. At the heart of their usefulness are apps written to help people with communication, productivity, and entertainment. Most of these apps are designed to maximise user-engagement time. Developers want to make sure users keep coming back to their apps. They design their apps to be addictive. 

**Smartphone addiction is a real problem. I want to help people manage the time they spend on their phones.**

# Introduction
There has been significant investment recently in this field by Google (Digital Wellbeing platform), Apple (Screen time), and many others. 
Several apps exist already to help users disconnect from their phones, and most of them have similar feature sets.

Such apps deal with a lot of sensitive personal information, but none of the existing solutions promise to honour user privacy 

I have inspected various popular existing solutions, and found the following flaws:

1. Closed source codebase, with no transparency about how user data is handled
2. Most of the existing solutions are either paid-only, or hide useful features behind a paywall.
3. Free solutions have shady user-data privacy policies.
4. Clunky user interfaces

My research into these apps can be found here: [Market Research](https://github.com/haroldadmin/GSOC-2019/blob/master/Rosewood/Market%20Research.md)

# My Proposal
A free, open source, privacy-friendly, good-looking app to help people manage their phone usage time. 

I believe that pursuit of a healthy digital life should be free, and not come at the expense of privacy. An open source solution to this problem would be the ideal solution, because open source code is inherently more trustworthy than closed-source code. I also want the user to be completely in-control of their data, and so I want to keep all usage statistics on device only. 

### MVP
I want to propose the following features in the minimum viable product:
1. Track how much time is spent on each app on the phone
2. Allow the user to set timers to limit the amount of time spent on each app.
3. Stop the user from accessing apps whose usage-time-limit has run out
4. A “Focus” mode which temporarily disables all notifications and calls
5. A scheduled Focus mode which turns on automatically at the time specified by the user
6. View a timeline of interactions with the device (a timeline of which app was interacted with at what time).
7. Export all data collected by the app into a text or CSV file on demand
8. Delete all data collected by the app on demand
9. Clean, easy to use user-interface

### Extensions
I also want to add some/all of the following features to this project to deliver a great user-experience, if mentors feel they are viable in the time frame of GSOC:
1. A backend server to sync user data and preferences such as app timers, so that when the user switches their device all their configuration can be restored.
2. A project extension to enable parent-child relationships in the app, where a parent can set app timers on their child’s phone, and also monitor their usage.
3. Integration with Google Fit service to show physical activities in the timeline as well. Also, this would allow us to show the user how much time they spent being physically active vs how much time they spent on their phones.
4. Recommendations to the user about applications to uninstall based on their frequency of use. 

I have worked on a related project in the past, by the name of [Rosewood](www.github.com/haroldadmin/rosewood), which is really useful experience for this project. To obtain usage statistics of the phone, I plan to use the `UsageStatsManager` class. To prevent the user from accessing apps whose timer has run out, I plan to use the `Accessibility Service` class

# Tech Stack
#### Android Application:
* Written in Kotlin
* MVVM Architecture pattern
* Room persistence library for persisting information in a local database
* Dependency injection through Koin
* RxJava/Kotlin Coroutines to manage streams of data and asynchronous operations

#### Backend server (if need be)
* A backend server written in ExpressJS (NodeJS) or Python-Flask, depending on mentor availability

# Finishing up
I want to work on this proposal because I feel the need for a way to manage my screen time in a privacy friendly way. Also judging by the popularity of the existing solutions which I mentioned in the Market Research document, I think there is also a huge market for this.

Any questions, feedback or comments are welcome.

You can find me at these places:

Twitter: @haroldadmin
Github: haroldadmin
Reddit: /u/theharolddev
