# Market Research
#rosewood #gsoc

## Competitors
### Google’s digital wellbeing
[Digital Wellbeing – Apps on Google Play](https://play.google.com/store/apps/details?id=com.google.android.apps.wellbeing&hl=en_IN)

#### Advantages
* Completely integrated with the Android system. Has spot on App Usage metrics, and this accuracy is difficult to achieve.
* Can block apps from launching if screen time limit is exceeded.
* Can detect number of unlocks and notifications per day.

#### Disadvantages
* Severely limited availability: only available on Pixel and Android One devices, running Android 9 Pie or above.
* Not open source: It is difficult to trust a closed source app with this much user data
- - - -
### Quality Time
[QualityTime - My Digital Diet - Apps on Google Play](https://play.google.com/store/apps/details?id=com.zerodesktop.appdetox.qualitytime&hl=en)

#### Advantages
* Very fully featured
* Very popular

#### Disadvantages
* Closed source
* Most features are behind a pay-wall
* Contains ads

- - - -

### Screen Time
[Screen Time - Phone Usage Tracker - Apps on Google Play](https://play.google.com/store/apps/details?id=com.agooday.screentime)

#### Advantages
* Supports app timers
* Free version has complete functionality

#### Disadvantages
* Poor user interface
* Ad supported
* Does not have usage timeline

- - - -

### YourHour
[Your Hour - phone addiction tracker and controller - Apps on Google Play](https://play.google.com/store/apps/details?id=com.mindefy.phoneaddiction.mobilepe)

#### Advantages
* Supports app timers
* Has usage timeline
* Has detailed statistics on app usage

#### Disadvantages
* Clunky user interface
* Ad supported
* Useful features hidden behind paywall

- - - -

### AntiSocial
[AntiSocial: phone addiction - Apps on Google Play](https://play.google.com/store/apps/details?id=com.goozix.antisocial_personal)

#### Advantages
* Allows comparisons to other users’ phone usage
* Supports app timers
* A lot of other little features

### Disadvantages
* Privacy policy mentions that user information is shared to third parties